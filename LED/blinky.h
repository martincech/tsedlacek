/*****************************************************************************
* Model: blinky.qm
* File:  ./blinky.h
*
* This code has been generated by QM tool (see state-machine.com/qm).
* DO NOT EDIT THIS FILE MANUALLY. All your changes will be lost.
*
* This program is open source software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published
* by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*****************************************************************************/
/*${.::blinky.h} ...........................................................*/
#ifndef blinky_h
#define blinky_h
#include "qpc.h"


enum BlinkySignals {
    TIMEOUT_SIG = Q_USER_SIG,
    button_SIG,
    MAX_SIG
};

void Blinky_ctor(void);
extern QActive * const AO_Blinky;

#endif /* blinky_h */
