/*
	This file contains the entry point (Reset_Handler) of your firmware project.
	The reset handled initializes the RAM and calls system library initializers as well as
	the platform-specific initializer and the main() function.
*/

extern void SystemInit(void);
extern void assert_failed(char const *module, int loc);

void __attribute__((naked, noreturn)) Reset_Handler() {
	extern int main(void);
	extern int __libc_init_array(void);
	extern unsigned __data_start;  /* start of .data in the linker script */
	extern unsigned __data_end__;  /* end of .data in the linker script */
	extern unsigned const __data_load; /* initialization values for .data  */
	extern unsigned __bss_start__; /* start of .bss in the linker script */
	extern unsigned __bss_end__;   /* end of .bss in the linker script */
	extern void software_init_hook(void) __attribute__((weak));

	unsigned const *src;
	unsigned *dst;

	SystemInit(); /* CMSIS system initialization */

	        /* copy the data segment initializers from flash to RAM... */
	src = &__data_load;
	for (dst = &__data_start; dst < &__data_end__; ++dst, ++src) {
		*dst = *src;
	}

	    /* zero fill the .bss segment in RAM... */
	for (dst = &__bss_start__; dst < &__bss_end__; ++dst) {
		*dst = 0;
	}

	    /* init hook provided? */
	if (&software_init_hook != (void(*)(void))(0)) {
	    /* give control to the RTOS */
		software_init_hook(); /* this will also call __libc_init_array */
	}
	else {
	    /* call all static constructors in C++ (harmless in C programs) */
		__libc_init_array();
		(void)main(); /* application's entry point; should never return! */
	}

	    /* the previous code should not return, but assert just in case... */
	assert_failed("Reset_Handler", __LINE__);
}
