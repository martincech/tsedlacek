#include "bsp.h"
#include "qpc.h"
#include "gpio.h"
#include "common.h"
#include "kbi.h"


#include "uart.h"



//smazani signalu TIMEOUT - zmena podle tlacitka

void KBI0_Task(void)
{
	QK_ISR_ENTRY();   /* inform QK about entering an ISR */
	
	BSP_ledOn();

	QK_ISR_EXIT();  /* inform QK about exiting an ISR */
	
}


void KBI1_Task(void)
{
	QK_ISR_ENTRY();   /* inform QK about entering an ISR */
	
	BSP_ledOff();

	QK_ISR_EXIT();  /* inform QK about exiting an ISR */
}



void KBI_set()
{
	
	//KBI_DeInit(KBI0);
	KBI_ConfigType  sKBIConfig = { 0 };

	for (int i = 0; i < KBI_MAX_PINS_PER_PORT; i++)
	{
		sKBIConfig.sPin[i].bEn	 = 0;
	}
/*
	sKBIConfig.sBits.bIntEn = 1; 
	sKBIConfig.sBits.bMode   = KBI_MODE_EDGE_ONLY;
	sKBIConfig.sPin[6].bEn   = 1;
	sKBIConfig.sPin[6].bEdge = KBI_RISING_EDGE_HIGH_LEVEL;
	
	sKBIConfig.sBits.bRstKbsp = 1;
	sKBIConfig.sBits.bIntEn = 1;
*/
	
   
	sKBIConfig.sBits.bRstKbsp   = 1; //Writing a 1 to RSTKBSP is to clear the KBIxSP Register
	sKBIConfig.sBits.bKbspEn   = 1;  //The latched value in KBxSP register while interrupt flag occur to be read.
	sKBIConfig.sBits.bMode   = KBI_MODE_EDGE_ONLY;
	sKBIConfig.sBits.bIntEn  = 1;    //interrupt enable

	sKBIConfig.sPin[6].bEdge = KBI_RISING_EDGE_HIGH_LEVEL;
	sKBIConfig.sPin[6].bEn   = 1;
	
	int i = sizeof(KBI_ConfigType);
	KBI_Init(KBI0, &sKBIConfig);
	//KBI_Init(KBI1, &sKBIConfig);

	KBI_SetCallback(KBI0, &KBI0_Task);
	//KBI_SetCallback(KBI1, &KBI1_Task);


}



void BSP_ledInit(void)
{
	/*
	GPIO_Init(GPIOA, GPIO_PTA7_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOA, GPIO_PTB0_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOA, GPIO_PTB5_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOA, GPIO_PTC0_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOA, GPIO_PTC1_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOA, GPIO_PTC2_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOA, GPIO_PTC3_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTF7_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTG5_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTG6_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTG7_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTH7_MASK, GPIO_PinOutput);
	
	GPIO_PinSet(GPIO_PTF7);
	GPIO_PinClear(GPIO_PTF7);
	*/
	//inicializace
	GPIO_Init(GPIOB, GPIO_PTG5_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTG6_MASK, GPIO_PinOutput);
	//GPIO_Init(GPIOA, GPIO_PTA6_MASK, GPIO_PinInput);
	
	//vypnut� po inicializaci
	GPIO_PinSet(GPIO_PTG5);
	GPIO_PinSet(GPIO_PTG6);
	
}
void BSP_ledOn(void) 
{
	//cteni registru
	uint32_t reg = GPIO_Read(GPIOA);
	if ((reg >> 6) & 1)
	{
		GPIO_PinSet(GPIO_PTG5);
		GPIO_PinClear(GPIO_PTG6);
	}
	else
	{
		GPIO_PinSet(GPIO_PTG6);
		GPIO_PinClear(GPIO_PTG5);
	}	
	
}


void BSP_ledOff(void) 
{
	//cteni registru
	uint32_t reg = GPIO_Read(GPIOA);
	if ((reg >> 6) & 1)
	{
		GPIO_PinSet(GPIO_PTG6);
	}
	else
	{
		GPIO_PinSet(GPIO_PTG5);
	}
   
	
}


void SysTick_Handler(void) {
	QK_ISR_ENTRY();   /* inform QK about entering an ISR */
	QF_TICK((void *)0); /* process time events for rate 0 */
	QK_ISR_EXIT();  /* inform QK about exiting an ISR */
}

/* QF callbacks ============================================================*/
void QF_onStartup(void) {
    /* set up the SysTick timer to fire at BSP_TICKS_PER_SEC rate */
	SysTick_Config(SystemCoreClock / BSP_TICKS_PER_SEC);

	    /* assing all priority bits for preemption-prio. and none to sub-prio. */
	//NVIC_SetPriorityGrouping(0U);

	    /* set priorities of ALL ISRs used in the system, see NOTE00
	    *
	    * !!!!!!!!!!!!!!!!!!!!!!!!!!!! CAUTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	    * Assign a priority to EVERY ISR explicitly by calling NVIC_SetPriority().
	    * DO NOT LEAVE THE ISR PRIORITIES AT THE DEFAULT VALUE!
	    */
	NVIC_SetPriority(SysTick_IRQn, QF_AWARE_ISR_CMSIS_PRI);
	/* ... */

	    /* enable IRQs... */
}
/*..........................................................................*/
void QF_onCleanup(void) {
}
/*..........................................................................*/
void QK_onIdle(void) {
    /* toggle LED1 on and then off, see NOTE01 */
	QF_INT_DISABLE();
	//BSP_ledOn();
	//BSP_ledOff();
	QF_INT_ENABLE();

#ifdef NDEBUG
	    /* Put the CPU and peripherals to the low-power mode.
	    * you might need to customize the clock management for your application,
	    * see the datasheet for your particular Cortex-M3 MCU.
	    */
	__WFI(); /* Wait-For-Interrupt */
#endif
}

/*..........................................................................*/
extern void assert_failed(char const *module, int loc);
void Q_onAssert(char const *module, int loc) {
    /*
    * NOTE: add here your application-specific error handling
    */
	(void)module;
	(void)loc;
	QS_ASSERTION(module, loc, (uint32_t)10000U); /* report assertion to QS */
	assert_failed(module, loc);
} 