#include "bsp.h"
#include "qpc.h"
#include <stdlib.h>

#include <stdio.h>

void KBI_init(void)
{

}

void BSP_ledInit(void)
{

}
void BSP_ledOff(void) {
	printf("LED OFF\n");
}
void BSP_ledOn(void) {
	printf("LED ON\n");
}

void Q_onAssert(char const Q_ROM * const Q_ROM_VAR file, int line) {
	fprintf(stderr, "Assertion failed in %s, line %d", file, line);
	exit(0);
}

void QF_onStartup(void) {}
void QF_onCleanup(void) {}
void QF_onClockTick(void) {
	QF_TICK((void *)0);
}
