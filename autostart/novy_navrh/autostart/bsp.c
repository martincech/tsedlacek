#include "bsp.h"
#include "qpc.h"
#include "autostart.h"
#include "signal.h"

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <stdarg.h>

#define LOW (0)
#define HIGH (1)
#define NUM_OF_REQUIREMENTS (4)
#define LENGTH_OF_LONGEST_REQUEST (21)
#define DIFFERENTIATION (10)
#define COOL_ON (11)
#define COOL_OFF (10)
#define LOW_BAT_ON (21)
#define START_UP_ON (31)
#define ELMOT_ON (41)
#define ELMOT_OFF (40)

//logging////////////////////////////////////////////////////////////////////
void log_signal_values(int diesel_on_request, int low_battery, int start_counter) {
	log_text("\t\t\tdiesel_on:%d  low_battery:%d\tstart_counter: %d\n", diesel_on_request, low_battery, start_counter);
}

void log_text(const char*fmt, ...) {
	va_list arg;
	va_start(arg, fmt);
	vprintf(fmt, arg);
	va_end(arg);
}

void log_blinky(int on, const char*funcName) {
}

//input values afret reset///////////////////////////////////////////////////
void input_values_of_signals(void) {
	const char requirements[NUM_OF_REQUIREMENTS][LENGTH_OF_LONGEST_REQUEST] = {
		{ "cool" },
		{ "low_battery" },
		{ "start up succesfully" },
		{ "elmot" }
	};
	char value_of_input[] = "0";
	for (int i = 0; i < NUM_OF_REQUIREMENTS; i++) {
		printf("\tsignal %s [0/1]:", requirements[i]);
		scanf("%s", value_of_input);
		int value_int = (atoi(value_of_input)) + ((i + 1) * DIFFERENTIATION);
		switch (value_int) {
		   case COOL_ON:  QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, COOL_ON_SIG)); break;
		   case COOL_OFF: QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, COOL_OFF_SIG)); break;
		   case LOW_BAT_ON: QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, LOW_BATTERY_SIG)); break;
		   case START_UP_ON: QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, START_UP_SUCCESFULLY_SIG)); break;
		   case ELMOT_ON:  QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, ELMOT_ON_SIG)); break;
		   case ELMOT_OFF: QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, ELMOT_OFF_SIG)); break;
		}
	}
}	
/////////////////////////////////////////////////////////////////////////////
//BSP functions
void BSP_start_diesel(void) {
	printf("\tstarting diesel engine...\n");
}
void BSP_stop_diesel(void) {
	printf("\tstopping diesel engine...\n");
}
void BSP_starting_error(void) {
	printf("\tstartup error or some other failure\n");
}
void BSP_error(void) {
	printf("\t-----------------------------unexpected error--------------------------\n");
}
/////////////////////////////////////////////////////////////////////////////
//reading signals from keyboard
void read_signal(void) {
	if (_kbhit()) {
		QEvt e;		
		uint8_t c;
		c = (uint8_t)_getch();
		switch (c) {
		case 'O':
		case 'o':  e.sig = START_UP_SUCCESFULLY_SIG; break;
		case 'P':
		case 'p':  e.sig = STARTING_FAILED_SIG; break;
		case 'B':
		case 'b':  e.sig = LOW_BATTERY_SIG; break;
		case 'N':
		case 'n':  e.sig = FULL_BATTERY_SIG; break;
		case 'M':
		case 'm':  e.sig = CHARGING_BATTERY_SIG; break;
		case 'V':
		case 'v':  e.sig = COOL_OFF_SIG; break;
		case 'C':
		case 'c':  e.sig = COOL_ON_SIG; break;
		case 'R':
		case 'r':  e.sig = ELMOT_OFF_SIG; break;
		case 'E':
		case 'e':  e.sig = ELMOT_ON_SIG; break;
		case 'Q':
		case 'q':  e.sig = RESET_FOR_DEBUG_SIG; break;
	   }
		QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, e.sig));
		//printf("\tsent to Autostart");
	}
}
/////////////////////////////////////////////////////////////////////////////

void QFSMInit() {
	SignalInit();  //starting automat for keyboar input
	AutostartInit(); //starting Autostart
}

void pin_init(void) {
	printf(" ----------------------------- \n");
	printf(" elmot on. . . . . . . . . e/E \n");
	printf(" elmot off . . . . . . . . r/R \n");
	printf(" cool on . . . . . . . . . c/C \n");
	printf(" cool off. . . . . . . . . v/V \n");
	printf(" low battery . . . . . . . b/B \n");
	printf(" full battery. . . . . . . n/N \n");
	printf(" charging battery. . . . . m/M \n");
	printf(" start up succesfully. . . o/O \n");
	printf(" startup failed. . . . . . p/P \n");
	printf(" reset for debug . . . . . q/Q \n");
	printf(" ----------------------------- \n");
}

void Q_onAssert(char const Q_ROM * const Q_ROM_VAR file, int line) {
	fprintf(stderr, "Assertion failed in %s, line %d", file, line);
	exit(0);
}

void QF_onStartup(void) {
	 }
void QF_onCleanup(void) {}
void QF_onClockTick(void) {
	QF_TICK((void *)0);
}