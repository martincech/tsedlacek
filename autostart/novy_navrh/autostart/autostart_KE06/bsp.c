#include "bsp.h"
#include "qpc.h"
#include "gpio.h"
#include "common.h"
#include "autostart.h"

#include <string.h>
#include <stdarg.h>

/*/////////////
ELMOT - PTA6
LOW_BATTERY - PTE4
FULL_BATTERY - PTF1
CHARGING_BATTERY - PTC7
COOL - PTF0
STARTUP_SUCCESFULLY - PTE3
STARTING_FAILED - PTE2

LD FLAGS: -specs=nano.specs -specs=nosys.specs    <- without ARM semihosting console
LD FLAGS: -specs=rdimon.specs  <- enable ARM semihosting console
///*/

#define LOW (0)
#define HIGH (1)
#define LOW_BATTERY (2)
#define FULL_BATTERY (3)
#define CHARGING_BATTERY (4)
#define ELMOT_ON (5)
#define ELMOT_OFF (6)
#define COOL_ON (7)
#define COOL_OFF (8)
#define START_UP_SUCCESFULLY (9)
#define STARTING_FAILED (10)
#define NO_CHANGES (13)
//////////////////////////////////////////////////////////////////////////
void input_values_of_signals(void) {}


///BSP functions//////////////////////////////////////////////////////////////////
void BSP_start_diesel(void) { 
	printf("\tstarting diesel...\n");
	GPIO_PinSet(GPIO_PTG5);
}
void BSP_stop_diesel(void) {
	printf("\tstoping diesel\n");
	GPIO_PinSet(GPIO_PTG6);
	GPIO_PinClear(GPIO_PTG5);
}
void BSP_starting_error(void) {
	printf("\tstarting error, too many starting request\n");
	GPIO_PinClear(GPIO_PTG5);
	GPIO_PinClear(GPIO_PTG6);
	GPIO_PinClear(GPIO_PTG7);
}
void BSP_error(void) {
	printf("\t-----------------------unexpected error------------------\n");
	GPIO_PinSet(GPIO_PTB0);
	GPIO_PinSet(GPIO_PTC1);
	GPIO_PinClear(GPIO_PTG5);
	GPIO_PinClear(GPIO_PTG6);
	GPIO_PinClear(GPIO_PTG7);
	GPIO_PinSet(GPIO_PTF7);
	GPIO_PinSet(GPIO_PTH7);	
}

//logging////////////////////////////////////////////////////////////////////////////////////
void log_signal_values(int low_battery, int diesel_on_request, int start_counter) {
	log_text("\t\tdiesel_on: %d  low_battery: %d \tstart_counter: %d\n", low_battery, diesel_on_request, start_counter);
}

void log_text(const char*fmt, ...) {
	va_list arg;
	va_start(arg, fmt);
	vprintf(fmt, arg);
	va_end(arg);
}

void log_blinky(int on, const char*funcName) {
	if (on == 1) {
		if (!(strcmp(funcName, "Autostart_elmot_on"))) {
			GPIO_PinSet(GPIO_PTH7);
		}
		else if (!(strcmp(funcName, "Autostart_cooling_request"))) {
			GPIO_PinSet(GPIO_PTF7);
		}
		else if (!(strcmp(funcName, "Autostart_starting_diesel"))) {
			GPIO_PinClear(GPIO_PTG7);
		}
		else if (!(strcmp(funcName, "Autostart_starting_failed_waiting"))) {
			GPIO_PinSet(GPIO_PTF7);
		}
		else if (!(strcmp(funcName, "Autostart_diesel_on"))) {
			GPIO_PinClear(GPIO_PTG6);
		}
		else if (!(strcmp(funcName, "Autostart_diesel_engine_cooling"))) {
			GPIO_PinSet(GPIO_PTF7);
		}
		else if (!(strcmp(funcName, "Autostart_elmot_off"))) {
			GPIO_PinSet(GPIO_PTB0);
			GPIO_PinSet(GPIO_PTG5);
		}
	}
	else {
		if (!(strcmp(funcName, "Autostart_elmot_on"))) {
			GPIO_PinClear(GPIO_PTH7);
		}
		else if (!(strcmp(funcName, "Autostart_cooling_request"))) {
			GPIO_PinClear(GPIO_PTF7);
		}
		else if (!(strcmp(funcName, "Autostart_starting_diesel"))) {
			GPIO_PinSet(GPIO_PTG7);
		}
		else if (!(strcmp(funcName, "Autostart_starting_failed_waiting"))) {
			GPIO_PinClear(GPIO_PTF7);
		}
		else if (!(strcmp(funcName, "Autostart_diesel_on"))) {
			GPIO_PinSet(GPIO_PTG6);
		}
		else if (!(strcmp(funcName, "Autostart_diesel_engine_cooling"))) {
			GPIO_PinClear(GPIO_PTF7);
		}
		else if (!(strcmp(funcName, "Autostart_elmot_off"))) {
			GPIO_PinClear(GPIO_PTB0);
		}
	}
}

void pin_init(void) {
   //input buttons
   //GPIOA INIT
	GPIO_Init(GPIOA, GPIO_PTA6_MASK, GPIO_PinInput_InternalPullup);
	GPIO_Init(GPIOA, GPIO_PTC6_MASK, GPIO_PinInput_InternalPullup);
	GPIO_Init(GPIOA, GPIO_PTC7_MASK, GPIO_PinInput_InternalPullup);
	//GPIOB INIT
	GPIO_Init(GPIOB, GPIO_PTE2_MASK, GPIO_PinInput_InternalPullup);
	GPIO_Init(GPIOB, GPIO_PTE3_MASK, GPIO_PinInput_InternalPullup);
	GPIO_Init(GPIOB, GPIO_PTE4_MASK, GPIO_PinInput_InternalPullup);
	GPIO_Init(GPIOB, GPIO_PTF0_MASK, GPIO_PinInput_InternalPullup);
	GPIO_Init(GPIOB, GPIO_PTF1_MASK, GPIO_PinInput_InternalPullup);

   //ledky
	GPIO_Init(GPIOA, GPIO_PTB0_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOA, GPIO_PTC1_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTG5_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTG6_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTG7_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTF7_MASK, GPIO_PinOutput);
	GPIO_Init(GPIOB, GPIO_PTH7_MASK, GPIO_PinOutput);
	//vypnut� led po inicializaci
	GPIO_PinClear(GPIO_PTB0);
	GPIO_PinClear(GPIO_PTC1);
	GPIO_PinSet(GPIO_PTG5);
	GPIO_PinSet(GPIO_PTG6);
	GPIO_PinSet(GPIO_PTG7);
	GPIO_PinClear(GPIO_PTF7);
	GPIO_PinClear(GPIO_PTH7);	
}

//reading GPIO//////////////////////////////////////////////////////
int32_t GPIOAread() {
 /* state of the button debouncing, see below */
	static struct ButtonsDebouncing {
		uint32_t depressed;
		uint32_t previous;
	} buttons = { 0, 1 };
	uint32_t current;
	uint32_t tmp;
	
	current = FGPIO_Read(FGPIOA); /* read GPIOA*/
	tmp = buttons.depressed; /* save the debounced depressed buttons */
	buttons.depressed |= (buttons.previous & current); /* set depressed */
	buttons.depressed &= (buttons.previous | current); /* clear released */
	buttons.previous   = current; /* update the history */
	tmp ^= buttons.depressed;     /* changed debounced depressed */
	if ((tmp & GPIO_PTA6_MASK)) {  /* debounced PTA6 state changed? */
		if ((buttons.depressed & GPIO_PTA6_MASK)) {/*PTA6 depressed?*/
			return ELMOT_ON;
		}
		else {
			return ELMOT_OFF;
		}
	}

	if ((tmp & GPIO_PTC7_MASK)) {  /* debounced ptC7 state changed? */
		if ((buttons.depressed & GPIO_PTC7_MASK)) {/*ptc7 signal?*/
			return CHARGING_BATTERY;
		}
	} 

	return NO_CHANGES;
}

int32_t GPIOBread() {
 /* state of the button debouncing, see below */
	static struct ButtonsDebouncing {
		uint32_t depressed;
		uint32_t previous;
	} buttons = { 0,  0xF};//16
	uint32_t current;
	uint32_t tmp;
	
	current = FGPIO_Read(FGPIOB); /* read GPIOB*/
	tmp = buttons.depressed; /* save the debounced depressed buttons */
	buttons.depressed |= (buttons.previous & current); /* set depressed */
	buttons.depressed &= (buttons.previous | current); /* clear released */
	buttons.previous   = current; /* update the history */
	tmp ^= buttons.depressed;     /* changed debounced depressed */
	if ((tmp & GPIO_PTE4_MASK)) {  /* debounced pte4 state changed? */
		if ((buttons.depressed & GPIO_PTE4_MASK)) {/*pte4 depressed?*/
			return LOW_BATTERY;
		}	
	}

	if ((tmp & GPIO_PTF0_MASK)) {  /* debounced ptf0 state changed? */
		if ((buttons.depressed & GPIO_PTF0_MASK)) {/*ptf0 depressed?*/
			return COOL_ON;
		}	 
		else {
			return COOL_OFF;
		}
	}

	if ((tmp & GPIO_PTF1_MASK)) {  /* debounced ptf1 state changed? */
		if ((buttons.depressed & GPIO_PTF1_MASK)) {/*ptf1 depressed?*/
			return FULL_BATTERY;
		}	
	}

	if ((tmp & GPIO_PTE3_MASK)) {  /* debounced pte3 state changed? */
		if ((buttons.depressed & GPIO_PTE3_MASK)) {/*pte3 depressed?*/
			return START_UP_SUCCESFULLY;
		}
	} 

	if ((tmp & GPIO_PTE2_MASK)) {  /* debounced pte2 state changed? */
		if ((buttons.depressed & GPIO_PTE2_MASK)) {/*pte2 depressed?*/
			return STARTING_FAILED;
		}	
	}

	return NO_CHANGES;
}

/////////////////////////////////////////////////////////////////////////////////
void SysTick_Handler(void) {
	QK_ISR_ENTRY();   /* inform QK about entering an ISR */
	QF_TICK((void *)0); /* process time events for rate 0 */

	int32_t GPIOAreg = GPIOAread();
	int32_t GPIOBreg = GPIOBread();
   
	switch (GPIOAreg) {
	   case ELMOT_OFF:
         QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, ELMOT_OFF_SIG));		         
		   break;
	   case ELMOT_ON:
		   QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, ELMOT_ON_SIG));		
		   break;
	   case CHARGING_BATTERY:
		   QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, CHARGING_BATTERY_SIG));		
		   break;	 
	   default:
		   break;
	}

	switch (GPIOBreg) {
	case LOW_BATTERY:
		QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, LOW_BATTERY_SIG));		         
		break;
	case FULL_BATTERY:
		QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, FULL_BATTERY_SIG));		
		break;
   case STARTING_FAILED:
		QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, STARTING_FAILED_SIG));		
		break;
	case START_UP_SUCCESFULLY:
		QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, START_UP_SUCCESFULLY_SIG));		
		break;
	case COOL_OFF:
		QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, COOL_OFF_SIG));		
		break;
	case COOL_ON:
		QActive_postFIFO(AO_Autostart, Q_NEW(QEvt, COOL_ON_SIG));		
		break;
	default:
		break;
	}

	QK_ISR_EXIT();  /* inform QK about exiting an ISR */
}

/* QF callbacks ============================================================*/
void QF_onStartup(void) {
    /* set up the SysTick timer to fire at BSP_TICKS_PER_SEC rate */
	SysTick_Config(SystemCoreClock / BSP_TICKS_PER_SEC);

	    /* assing all priority bits for preemption-prio. and none to sub-prio. */
	//NVIC_SetPriorityGrouping(0U);

	    /* set priorities of ALL ISRs used in the system, see NOTE00
	    *
	    * !!!!!!!!!!!!!!!!!!!!!!!!!!!! CAUTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	    * Assign a priority to EVERY ISR explicitly by calling NVIC_SetPriority().
	    * DO NOT LEAVE THE ISR PRIORITIES AT THE DEFAULT VALUE!
	    */
	NVIC_SetPriority(SysTick_IRQn, QF_AWARE_ISR_CMSIS_PRI);
	/* ... */

	    /* enable IRQs... */
}
/*..........................................................................*/
void QF_onCleanup(void) {
}
/*..........................................................................*/
void QK_onIdle(void) {
    /* toggle LED1 on and then off, see NOTE01 */
	QF_INT_DISABLE();
	
	QF_INT_ENABLE();

#ifdef NDEBUG
	    /* Put the CPU and peripherals to the low-power mode.
	    * you might need to customize the clock management for your application,
	    * see the datasheet for your particular Cortex-M3 MCU.
	    */
	__WFI(); /* Wait-For-Interrupt */
#endif
}

/*..........................................................................*/
extern void assert_failed(char const *module, int loc);
void Q_onAssert(char const *module, int loc) {
    /*
    * NOTE: add here your application-specific error handling
    */
	(void)module;
	(void)loc;
	QS_ASSERTION(module, loc, (uint32_t)10000U); /* report assertion to QS */
	assert_failed(module, loc);
} 

void QFSMInit() {
	AutostartInit();  //starting Autostart
}