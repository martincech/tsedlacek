/*
   This file contains the definitions of the interrupt handlers for KL25Z4 MCU family.
   The file is provided by Sysprogs under the BSD license.
*/

#include "MKE06Z4.h"
#define NULL ((void *)0)
/* start and end of stack defined in the linker script ---------------------*/
extern int __stack_start__;
extern int __stack_end__;
/* Weak prototypes for error handlers --------------------------------------*/
/**
* \note
* The function assert_failed defined at the end of this file defines
* the error/assertion handling policy for the application and might
* need to be customized for each project. This function is defined in
* assembly to avoid accessing the stack, which might be corrupted by
* the time assert_failed is called.
*/
__attribute__((naked)) void assert_failed(char const *module, int loc);

/* Function prototypes -----------------------------------------------------*/
void Default_Handler(void);  /* Default empty handler */
void Reset_Handler(void);    /* Reset Handler */
void SystemInit(void);       /* CMSIS system initialization */

/*----------------------------------------------------------------------------
* weak aliases for each Exception handler to the Default_Handler.
* Any function with the same name will override these definitions.
*/
/* Cortex-M Processor fault exceptions... */
void NMI_Handler              (void) __attribute__((weak));
void HardFault_Handler        (void) __attribute__((weak));

/* Cortex-M Processor non-fault exceptions... */
void SVC_Handler              (void) __attribute__((weak, alias("Default_Handler")));
void PendSV_Handler           (void) __attribute__((weak, alias("Default_Handler")));
void SysTick_Handler          (void) __attribute__((weak, alias("Default_Handler")));

/* external interrupts...   */
void FTMRE_Handler(void) __attribute__((weak, alias("Default_Handler")));
void LVD_LLW_Handler(void) __attribute__((weak, alias("Default_Handler")));
void IRQ_Handler(void) __attribute__((weak, alias("Default_Handler")));
void I2C0_Handler(void) __attribute__((weak, alias("Default_Handler")));
void I2C1_Handler(void) __attribute__((weak, alias("Default_Handler")));
void SPI0_Handler(void) __attribute__((weak, alias("Default_Handler")));
void SPI1_Handler(void) __attribute__((weak, alias("Default_Handler")));
void UART0_Handler(void) __attribute__((weak, alias("Default_Handler")));
void UART1_Handler(void) __attribute__((weak, alias("Default_Handler")));
void UART2_Handler(void) __attribute__((weak, alias("Default_Handler")));
void ADC0_Handler(void) __attribute__((weak, alias("Default_Handler")));
void ACMP0_Handler(void) __attribute__((weak, alias("Default_Handler")));
void FTM0_Handler(void) __attribute__((weak, alias("Default_Handler")));
void FTM1_Handler(void) __attribute__((weak, alias("Default_Handler")));
void FTM2_Handler(void) __attribute__((weak, alias("Default_Handler")));
void RTC_Handler(void) __attribute__((weak, alias("Default_Handler")));
void ACMP1_Handler(void) __attribute__((weak, alias("Default_Handler")));
void PIT_CH0_Handler(void) __attribute__((weak, alias("Default_Handler")));
void PIT_CH1_Handler(void) __attribute__((weak, alias("Default_Handler")));
void KBI0_Handler(void) __attribute__((weak, alias("Default_Handler")));
void KBI1_Handler(void) __attribute__((weak, alias("Default_Handler")));
void ICS_Handler(void) __attribute__((weak, alias("Default_Handler")));
void Watchdog_Handler(void) __attribute__((weak, alias("Default_Handler")));
void PWT_Handler(void) __attribute__((weak, alias("Default_Handler")));
void MSCAN_RX_Handler(void) __attribute__((weak, alias("Default_Handler")));
void MSCAN_TX_Handler(void) __attribute__((weak, alias("Default_Handler")));

/*..........................................................................*/
__attribute__((section(".isr_vector")))
int const g_pfnVectors[] = {
   /* Core interrupts */
   (int)&__stack_end__,          /* Top of Stack                    */
   (int)&Reset_Handler,          /* Reset Handler                   */
   (int)&NMI_Handler,            /* NMI Handler                     */
   (int)&HardFault_Handler,      /* Hard Fault Handler              */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)&SVC_Handler,            /* SVCall handler                  */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                            /* Reserved                        */
   (int)&PendSV_Handler,         /* The PendSV handler              */
   (int)&SysTick_Handler,        /* The SysTick handler             */
   /* Device specific interrupts */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)NULL,                         /* Reserved                        */
   (int)&FTMRE_Handler,                /**< FTMRE command complete */
   (int)&LVD_LLW_Handler,                /**< Low-voltage warning */
   (int)&IRQ_Handler,                /**< External interrupt */
   (int)&I2C0_Handler,                /**< I2C0 single interrupt vector for all sources */
   (int)&I2C1_Handler,                /**< I2C1 single interrupt vector for all sources */
   (int)&SPI0_Handler,               /**< SPI0 single interrupt vector for all sources */
   (int)&SPI1_Handler,               /**< SPI1 single interrupt vector for all sources */
   (int)&UART0_Handler,               /**< UART0 status and error */
   (int)&UART1_Handler,               /**< UART1 status and error */
   (int)&UART2_Handler,               /**< UART2 status and error */
   (int)&ADC0_Handler,               /**< ADC conversion complete interrupt */
   (int)&ACMP0_Handler,               /**< ACMP0 interrupt */
   (int)&FTM0_Handler,               /**< FTM0 single interrupt vector for all sources */
   (int)&FTM1_Handler,               /**< FTM1 single interrupt vector for all sources */
   (int)&FTM2_Handler,               /**< FTM2 single interrupt vector for all sources */
   (int)&RTC_Handler,               /**< RTC overflow */
   (int)&ACMP1_Handler,               /**< ACMP1 interrupt */
   (int)&PIT_CH0_Handler,               /**< PIT CH0 overflow */
   (int)&PIT_CH1_Handler,               /**< PIT CH1 overflow */
   (int)&KBI0_Handler,               /**< KBI0 interrupt */
   (int)&KBI1_Handler,               /**< KBI1 interrupt */
   (int)NULL,                         /* Reserved                        */
   (int)&ICS_Handler,               /**< Clock loss of lock */
   (int)&Watchdog_Handler,               /**< Watchdog timeout */
   (int)&PWT_Handler,               /**< PWT interrupt */
   (int)&MSCAN_RX_Handler,               /**< MSCAN Rx interrupt */
   (int)&MSCAN_TX_Handler                /**< MSCAN Tx, Err and Wake-up interrupt */
};



/* fault exception handlers ------------------------------------------------*/
__attribute__((naked)) void NMI_Handler(void);
void NMI_Handler(void) {
   __asm volatile(
       "    ldr r0,=str_nmi\n\t"
       "    mov r1,#1\n\t"
       "    b assert_failed\n\t"
       "str_nmi: .asciz \"NMI\"\n\t"
   );
}

/*..........................................................................*/
__attribute__((naked)) void HardFault_Handler(void);
void HardFault_Handler(void) {
   __asm volatile(
       "    ldr r0,=str_hrd\n\t"
       "    mov r1,#1\n\t"
       "    b assert_failed\n\t"
       "str_hrd: .asciz \"HardFault\"\n\t"
   );
}

/*..........................................................................*/
__attribute__((naked)) void Default_Handler(void);
void Default_Handler(void) {
   __asm volatile(
       "    ldr r0,=str_dflt\n\t"
       "    mov r1,#1\n\t"
       "    b assert_failed\n\t"
       "str_dflt: .asciz \"Default\"\n\t"
   );
}

/****** End Of File *********************************************************/
