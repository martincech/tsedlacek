#Generated by VisualGDB (http://visualgdb.com)
#DO NOT EDIT THIS FILE MANUALLY UNLESS YOU ABSOLUTELY NEED TO
#USE VISUALGDB PROJECT PROPERTIES DIALOG INSTEAD

BINARYDIR := Debug

#Toolchain
CC := C:/SysGCC/MinGW32/bin/gcc.exe
CXX := C:/SysGCC/MinGW32/bin/g++.exe
LD := $(CXX)
AR := C:/SysGCC/MinGW32/bin/ar.exe
OBJCOPY := C:/SysGCC/MinGW32/bin/objcopy.exe

#Additional flags
PREPROCESSOR_MACROS := DEBUG
INCLUDE_DIRS := . .. ./QPC ../QPC/include ../QPC/src
LIBRARY_DIRS := 
LIBRARY_NAMES := 
ADDITIONAL_LINKER_INPUTS := 
MACOS_FRAMEWORKS := 
LINUX_PACKAGES := 

CFLAGS := -ggdb -ffunction-sections -O0 -std=c99
CXXFLAGS := -ggdb -ffunction-sections -O0 -std=c99
ASFLAGS := 
LDFLAGS := -Wl,-gc-sections
COMMONFLAGS := 
LINKER_SCRIPT := 

START_GROUP := -Wl,--start-group
END_GROUP := -Wl,--end-group

#Additional options detected from testing the toolchain
USE_DEL_TO_CLEAN := 1
CP_NOT_AVAILABLE := 1
